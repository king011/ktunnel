#!/bin/bash
#Program:
#       c build scripts
#
#Email:
#       zuiwuchang@gmail.com
dir=`cd $(dirname $BASH_SOURCE) && pwd`

function check(){
	if [ "$1" != 0 ] ;then
		exit $1
	fi
}
function mkDir(){
	mkdir -p "$1"
	check $?
}

function newFile(){
	echo "$2" > "$1"
	check $?
}
function writeFile(){
	echo "$2" >> "$1"
	check $?
}
function Version(){
	filename=$dir/version.h

	tag=`git describe`
	if [ "$tag" == '' ];then
		tag="[unknown tag]"
	fi

	commit=`git rev-parse HEAD`
	if [ "$commit" == '' ];then
		commit="[unknow commit]"
	fi
	
	date=`date +'%Y-%m-%d %H:%M:%S'`

	echo $tag $commit
	echo $date

	newFile $filename	'#ifndef _C_K_TUNNEL_'
	writeFile $filename	'#define _C_K_TUNNEL_'
	writeFile $filename	''
	writeFile $filename	"#define K_TUNNEL_BUILD_TAG \"$tag\""
	writeFile $filename	"#define K_TUNNEL_BUILD_COMMIT \"$commit\""
	writeFile $filename	"#define K_TUNNEL_BUILD_DATE \"$date\""
	writeFile $filename	''
	writeFile $filename	'#endif // _C_K_TUNNEL_'
}

function ShowHelp(){
	echo "help              : show help"
	echo "cmake [r/d]       : cmake"
	echo "make  [r/d]  [4]  : make"
	echo "clear [r/d]       : clear"
}
function CMake(){
    if [[ "$1" == "r" ]];then
        local pwd="$dir/output/release"
    else
        local pwd="$dir/output/debug"
    fi

    if [ ! -d "$pwd" ]; then
        mkdir -p "$pwd"
    fi

    if [[ "$1" == "r" ]];then
        cd $pwd && cmake -DCMAKE_BUILD_TYPE=Release ../../
    else
        cd $pwd && cmake -DCMAKE_BUILD_TYPE=Debug ../../
    fi
}
function Make(){
    if [[ "$1" == "r" ]];then
        local pwd="$dir/output/release"
    else
        local pwd="$dir/output/debug"
    fi

    if [ -d "$pwd" ]; then
        if [[ $2 == "" ]];then
            cd $pwd && make -j 4
        else
            cd $pwd && make -j $2
        fi
    fi
}
function Clear(){
    if [[ "$1" == "r" ]];then
        local pwd="$dir/output/release"
    else
        local pwd="$dir/output/debug"
    fi

    if [ -d "$pwd" ]; then
        cd $pwd && rm * -rf
    fi
}
case $1 in
	cmake)
        CMake $2
	;;

    make)
        Version
        Make $2 $3
	;;

    clear)
        Clear $2
	;;

	*)
		ShowHelp
	;;
esac