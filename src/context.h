#ifndef _C_K_TUNNEL_SRC_CONTEXT_
#define _C_K_TUNNEL_SRC_CONTEXT_

/**
* \brief 原始讀取函數
*
* 此回調函數 從原始設備中 獲取到數據  
*
* \param userdata 用戶 自定義的 接口關聯數據 
* \param buffer 數據被讀取到此緩衝區中
* \param size 緩衝區 buffer 大小
* \return 返回讀取到的 數據長度
* * 如果 返回值 < 0 則代表 通道出錯 應該關閉通道\n
* * 如果 返回值 == 0 則代表 沒有可讀數據\n
* * 如果 返回值 > 0 則爲實際讀取到 buffer 中的 數據
*
* \warning buffer 中一旦讀取到任何數據 返回值 必須是 讀取到的數據長度 如果此時 通道已經出錯 應該在下次讀取時 再直接返回錯誤
*/
typedef int (*raw_read_ft)(void *userdata, const char buffer, int size);
/**
*	\brief 原始寫入函數
*
* 此回調函數 將數據寫入到 原始設備中
*
* \param userdata 用戶 自定義的 接口關聯數據 
* \param buffer 要寫入的數據緩衝區
* \param size 要寫入的 緩衝區 大小
* \return 實際寫入的 數據長度
*
* \warning buffer 中一旦有任何數據寫入成功 返回值 必須是 實際寫入的數據長度 如果此時 通道已經出錯 應該在下次寫入時 再直接返回錯誤
*/
typedef int (*raw_write_ft)(void *userdata, const char buffer, int size);

/**
*	\brief 隧道 執行環境
*/
typedef struct _context_t
{
    raw_read_ft read;
    raw_write_ft write;
    void *userdata;
} c_k_tunnel_context_t;
/**
*	\brief 創建 隧道
*
* \param size 數據包最大尺寸 如果 <1 則使用默認值 384
* \param write 原始寫入接口
* \param read 原始讀取接口
* \param userdata 用戶 自定義的 接口關聯數據 
* \param e 如果非空 用於返回 錯誤碼
* \return 如果創建成功 返回 執行環境 否則 返回錯誤碼到 e 中
*
* \warning 不要調用 free 應該 調用 c_k_tunnel_context_free 來釋放 隧道
* \warning size 是包 body 大小 網路數據包 應該再 + 傳輸協議包頭 +  c_k_tunnel包頭
*/
c_k_tunnel_context_t *c_k_tunnel_context_init(
    int size,
    raw_write_ft write,
    raw_read_ft read,
    void *userdata,
    int *e);

/**
* \brief 發包函數
*
* 將數據寫入到 發包緩衝區中
*
* \param buffer 要發送的數據緩衝區
* \param size 緩衝區 buffer 大小
* \return 實際寫入到緩衝區中的大小
* * 如果 返回值 < 0 則代表 寫入錯誤 應該釋放通道\n
* * 如果 返回值 == 0 則代表 寫入緩衝區已滿\n
* * 如果 返回值 > 0 則爲實際讀寫入到緩衝區 中的 數據 長度
*
*/
int c_k_tunnel_context_write(const char buffer, int size);
/**
* \brief 收包函數
*
* 從緩衝區中 收取數據包
*
* \param buffer 收包緩衝區
* \param size 緩衝區 buffer 大小
* \return 實際收取到緩衝區數據大小
* * 如果 返回值 < 0 則代表 錯誤 應該釋放通道\n
* * 如果 返回值 == 0 則代表 沒有任何數據\n
* * 如果 返回值 > 0 則爲實際讀寫入到緩衝區 中的 數據 長度
*
*/
int c_k_tunnel_context_read(const char buffer, int size);
/**
*	\brief 關閉 隧道 寫入接口
*
* \param context 調用 c_k_tunnel_context_init 返回的 隧道句柄
* \return 錯誤代碼
* 
* \note 關閉寫入接口 會導致 無法再將任何數據 提交到隧道中 已經進入隧道 緩衝區中的數據 將正常被發送
*/
int c_k_tunnel_context_shutdown_write(c_k_tunnel_context_t *context);
/**
*	\brief 關閉 隧道 讀取接口
*
* \param context 調用 c_k_tunnel_context_init 返回的 隧道句柄
* \return 錯誤代碼
* 
* \note 關閉讀取接口 隧道不會再調用 raw_read_ft 從 底層獲取數據 已經讀入到 隧道緩衝區的內容將可繼續被讀出
*/
int c_k_tunnel_context_shutdown_read(c_k_tunnel_context_t *context);
/**
*	\brief 關閉 隧道
*
* \param context 調用 c_k_tunnel_context_init 返回的 隧道句柄
* \return 錯誤代碼
* 
* \note 關閉隧道 會向遠端發送一個 close 包 以通知遠端 可以關閉隧道
* \note c_k_tunnel_context_close 會自動調用 c_k_tunnel_context_shutdown_write() c_k_tunnel_context_shutdown_read()
*/
int c_k_tunnel_context_close(c_k_tunnel_context_t *context);

/**
*	\brief 釋放 隧道
*
* \param context 調用 c_k_tunnel_context_init 返回的 隧道句柄
* \note c_k_tunnel_context_free 會自動調用 c_k_tunnel_context_close
* \warning 不要調用 重複調用 c_k_tunnel_context_free
*/
void c_k_tunnel_context_free(c_k_tunnel_context_t *context);
#endif // _C_K_TUNNEL_SRC_CONTEXT_