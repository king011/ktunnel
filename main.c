#include <stdio.h>
#include <string.h>
#include "version.h"
#include "src/context.h"
void display_help();
void display_version();
int run_server();
int run_server_wr(c_k_tunnel_context_t *ctx);
int run_client();
int main(int argc, char **argv)
{
    int e = 0;
    int help = 0;
    int version = 0;
    int server = 0;
    int client = 0;
    for (int i = 0; i < argc; i++)
    {
        if (!strcmp(argv[i], "-v"))
        {
            version = 1;
        }
        else if (!strcmp(argv[i], "-h"))
        {
            help = 1;
        }
        else if (!strcmp(argv[i], "-s"))
        {
            server = 1;
        }
        else if (!strcmp(argv[i], "-c"))
        {
            client = 1;
        }
    }

    if (help)
    {
        display_help();
    }
    else if (version)
    {
        display_version();
    }
    else if (server)
    {
        e = run_server();
    }
    else if (client)
    {
        e = run_client();
    }
    else
    {
        display_help();
        e = 1;
    }
    return e;
}
void display_help()
{
    puts("-v : display version");
    puts("-h : display help");
    puts("-s : run example server");
    puts("-c : run example client");
}
void display_version()
{
    printf("Tag = %s\n", K_TUNNEL_BUILD_TAG);
    printf("Commit = %s\n", K_TUNNEL_BUILD_COMMIT);
    printf("Date = %s\n", K_TUNNEL_BUILD_DATE);
}
int run_server()
{
    int e = 0;
    c_k_tunnel_context_t *ctx = c_k_tunnel_context_init(
        1,
        0, 0,
        0,
        &e);
    if (e == 0)
    {
        puts("c_k_tunnel_context_init success");

        // 模式收發 數據
        run_server_wr(ctx);

        // 通知 遠端 關閉隧道
        puts("c_k_tunnel_context_close");
        c_k_tunnel_context_close(ctx);

        // 通常可以 等待一段時間後 再 close
        // 如果立刻 close c_k_tunnel_context_close 發送的數據包 丟失 將不會被回傳
        // 此時 遠端只有 等待超時 才能知道 連接異常

        // 釋放 隧道資源
        puts("c_k_tunnel_context_free");
        c_k_tunnel_context_free(ctx);
    }
    else
    {
        printf("c_k_tunnel_context_init error : %d", e);
    }
    return e;
}
int run_server_wr(c_k_tunnel_context_t *ctx)
{
    // 已經 發送的 數據包
    int write = 0;
    // 已經 接收的 數據包
    int read = 0;
    while (write < 100 || read < 100)
    {
        // 如果 需要 發包
        if (write < 100)
        {
            // 發包
        }
    }
}
int run_client()
{
    puts("run_client");
}